﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public float survival_time = 0f;
   


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;

    }


    void Update ()
    {
        survival_time += Time.deltaTime;
       
        text.text = "Time Survived: " + survival_time.ToString("F2");
    }
}
